app.factory('Data', function($resource) {
    return $resource('/data/:id', { id: '@_id' }, {
        'update' : {
            method : 'PUT'
        }
    });
});