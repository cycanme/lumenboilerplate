app.factory('Person', function($resource) {
    return $resource('/person/:id', { id: '@_id' }, {
        'update' : {
            method : 'PUT'
        }
    });
});