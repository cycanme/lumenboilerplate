var app = angular.module('app',['ngRoute','ngResource']);

app.config(function($routeProvider){

    $routeProvider.when('/', {
        templateUrl : 'templates/index.html',
        controller: 'IndexController'
    }).when('/persons', {
        templateUrl : 'templates/persons.html',
        controller: 'IndexController'
    }).when('/persons/:personId', {
        templateUrl : 'templates/persons_edit.html',
        controller: 'PersonDetailController'
    }).when('/contact', {
        templateUrl : 'templates/contact.html',
        controller: 'IndexController'
    }).otherwise({
        redirectTo: '/'
    });

});