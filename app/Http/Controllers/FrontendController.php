<?php namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class FrontendController extends BaseController
{
    public function index(){
        return view('index');
    }
}
