<?php namespace App\Http\Controllers;

use App\Models\Person;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class DataController extends BaseController
{

    public function getData($limit=1000, $offset=0){

        $response = new JsonResponse();
        $data = Person::take($limit)->skip($offset)->get();
        $response->setData($data);
        return $response;
    }

    public function getPerson($personId){
        $response = new JsonResponse();
        $person = Person::find($personId);
        $response->setData($person);
        return $response;
    }

}