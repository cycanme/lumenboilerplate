<?php namespace App\Http\Middleware;

use Closure;

class CorsMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = new \Illuminate\Http\JsonResponse();
        $response->header("Access-Control-Allow-Origin", "*");
        $response->header("Access-Control-Allow-Methods","POST, GET, OPTIONS, PUT, DELETE");
        $response->header("Access-Control-Allow-Headers","X-Requested-With, content-type");
        $response->sendHeaders();
        return $next($request);
    }

}
