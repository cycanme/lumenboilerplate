<?php

/*
|--------------------------------------------------------------------------
| Application Routes Test
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$app->group(['namespace' => 'App\Http\Controllers', 'middleware' => []], function($app) {
    $app->get('/', ['as' => 'index', 'uses' => 'FrontendController@index']);
});

$app->group(['namespace' => 'App\Http\Controllers', 'middleware' => []], function($app) {
    $app->get('/persons', ['as' => 'persons', 'uses' => 'DataController@getData']);
    $app->get('/persons/{limit}/{offset}', ['as' => 'persons', 'uses' => 'DataController@getData']);
    $app->get('/person/{personId}', ['as' => 'person', 'uses' => 'DataController@getPerson']);
});


$app->get('test', function(){
    require_once(dirname(__FILE__).'/../Components/getid3/getid3.php');
    $g = new getID3();
    $r = $g->analyze('c:\temp\test2.mp4');
    \App\Debug::show($r);

});