<?php namespace App;
/**
 * Created by PhpStorm.
 * User: maiko.tepe
 * Date: 04.02.2016
 * Time: 09:08
 */
class Debug
{

    protected static $infos = [];
    private static $counter = 1;

    public static function add($value=null){

        $class = "N/A";
        $line = -1;

        $backtrace = debug_backtrace();
        if (isset($backtrace[1]['class'])) {
            $class =  $backtrace[1]['class'];
        }else{
            $class =  $backtrace[0]['file'];
        }
        $line = $backtrace[0]['line'];
        $i = str_pad(self::$counter++,2,STR_PAD_LEFT,0);
        self::$infos[$i.' '.$class.'@'.$line] = $value;

    }

    public static function show($value=null){

        $isJson = false;
        foreach(headers_list() as $k=>$h){
            if (strpos($h,'application/json') > 0){
                $isJson = true;
                break;
            }
        }

        if (!is_null($value)) {
            $backtrace = debug_backtrace();
            if (isset($backtrace[1]['class'])) {
                $class =  $backtrace[1]['class'];
            }else{
                $class =  $backtrace[0]['file'];
            }
            $line = $backtrace[0]['line'];
            $i = str_pad(self::$counter++,2,STR_PAD_LEFT,0);
            self::$infos[$i.' '.$class.'@'.$line] = $value;
        }
        if ($isJson) {
            echo json_encode(self::$infos);
        }else{
            var_dump(self::$infos);
        }
        exit();

    }

}